package com.application.cardGame.entities.gameRules.games;

import com.application.cardGame.entities.desk.ClassicCard;
import com.application.cardGame.entities.desk.Desk;
import com.application.cardGame.entities.gameRules.Game;
import com.application.cardGame.entities.gameRules.GameRules;
import com.application.cardGame.entities.players.Player;
import java.util.*;
import java.util.stream.Collectors;

public class MoreScore extends Game implements GameRules {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  List<Player> players;
  Desk desk = new Desk();
  Map<String, Integer> scores = new HashMap<String, Integer>();

  public MoreScore(List<Player> players) {
    super(4);
    if (this.maxPlayersNumber >= players.size() && players.size() > 0) {
      this.players = players;
    } else {
      throw new Error("Size players is incorrect.");
    }
  }

  public void calculateScore() {
    getAllScore();

    orderByScore();

    for (Map.Entry<String, Integer> entry : scores.entrySet()) {
      System.out.println(entry.getKey() + " => " + entry.getValue());
    }
  }

  private void orderByScore() {
    scores =
      scores
        .entrySet()
        .stream()
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .collect(
          Collectors.toMap(
            Map.Entry::getKey,
            Map.Entry::getValue,
            (oldValue, newValue) -> oldValue,
            LinkedHashMap::new
          )
        );
  }

  private void getAllScore() {
    for (Player player : players) {
      ClassicCard card = (ClassicCard) player.getCard().showCard();
      scores.put(player.getName(), card.getNumberCard());
    }
  }

  public void beginPartyGame() {
    for (Player player : players) {
      player.setCard(desk.getRandomCard());
    }
  }

  public void endPartyGame() {
    calculateScore();
  }

  public void restartGame() {
    for (Player player : players) {
      player.setCard(null);
    }
  }
}
