package com.application.cardGame.entities.gameRules;

public class Game extends Throwable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  protected int minPlayersNumber = 0;
  protected int maxPlayersNumber;

  public Game(int maxPlayersNumber) {
    this.maxPlayersNumber = maxPlayersNumber;
  }

  public Game(int minPlayersNumber, int maxPlayersNumber) {
    this.minPlayersNumber = minPlayersNumber;
    this.maxPlayersNumber = maxPlayersNumber;
  }
}
