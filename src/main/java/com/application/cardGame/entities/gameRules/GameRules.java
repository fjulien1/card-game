package com.application.cardGame.entities.gameRules;

import com.application.cardGame.communs.interfaces.CardInterface;

public interface GameRules {
  void calculateScore();
  void beginPartyGame();
  void endPartyGame();
}
