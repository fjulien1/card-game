package com.application.cardGame.entities.players;

import java.util.List;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class PlayersController {

  final String urlPlayers = "/players";
  final PlayerListModel players = new PlayerListModel();

  @GetMapping(urlPlayers)
  public ResponseEntity<List<Player>> getPlayers() {
    try {
      return new ResponseEntity<List<Player>>(players.getList(), HttpStatus.OK);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
    }
  }

  @GetMapping(urlPlayers + "/{id}")
  public ResponseEntity<Player> getOnePlayers(@PathVariable Long id) {
    try {
      return new ResponseEntity<Player>(players.foundOne(id), HttpStatus.OK);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
    }
  }

  @PostMapping(urlPlayers)
  public ResponseEntity<Long> postPlayers(
    @RequestParam(value = "name") String name
  ) {
    try {
      Player player = players.addPlayer(name);
      return new ResponseEntity<Long>(player.getId(), HttpStatus.CREATED);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
    }
  }

  @PutMapping(urlPlayers + "/{id}")
  public ResponseEntity<String> changeName(
    @RequestBody Player body,
    @PathVariable Long id
  ) {
    try {
      Player player = players.foundOne(id);
      if (player == null) {
        return new ResponseEntity<String>("NO UPDATE", HttpStatus.NOT_FOUND);
      }
      player.setName(body.getName());
      return new ResponseEntity<String>("UPDATED", HttpStatus.OK);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
    }
  }

  @DeleteMapping(urlPlayers + "/{id}")
  public ResponseEntity<String> deletePlayer(@PathVariable Long id) {
    try {
      players.remove(id);
      return new ResponseEntity<String>("DELETED", HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
    }
  }
  // private static final String template = "Hello, %s!";
  // private final AtomicLong counter = new AtomicLong();
  // @GetMapping("/greeting2")
  // public Player greeting2(
  //   @RequestParam(value = "name", defaultValue = "World") String name
  // ) {
  //   return new Player(
  //     counter.incrementAndGet(),
  //     String.format(template, name)
  //   );
  // }
}
