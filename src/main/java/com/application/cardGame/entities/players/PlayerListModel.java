package com.application.cardGame.entities.players;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class PlayerListModel {

  List<Player> list = new ArrayList<Player>();
  private final AtomicLong counter = new AtomicLong();

  public Player addPlayer(String name) {
    Player newPlayer = new Player(this.counter.incrementAndGet(), name);
    this.list.add(newPlayer);
    return newPlayer;
  }

  public List<Player> getList() {
    return list;
  }

  public void remove(Long id) {
    list =
      list
        .stream()
        .filter(player -> !id.equals(player.getId()))
        .collect(Collectors.toList());
  }

  Player foundOne(Long id) {
    return list
      .stream()
      .filter(playerFound -> id.equals(playerFound.getId()))
      .findAny()
      .orElse(null);
  }
}
