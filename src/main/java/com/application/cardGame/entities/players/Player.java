package com.application.cardGame.entities.players;

import com.application.cardGame.communs.FormatSQL;
import com.application.cardGame.communs.interfaces.Card;
import com.application.cardGame.communs.interfaces.User;

public class Player implements FormatSQL, User {

  private final long id;
  private String name;
  private Card card;

  public Player(long id, String name) {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Card getCard() {
    return card;
  }

  public void setCard(Card card) {
    this.card = card;
  }
}
