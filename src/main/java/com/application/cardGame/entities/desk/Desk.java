package com.application.cardGame.entities.desk;

import com.application.cardGame.constants.classicCard.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Desk {

  private List<ClassicCard> listCards;

  public Desk() {
    this.initDesk();
  }

  public void initDesk() {
    listCards = new ArrayList<ClassicCard>();
    for (SuitsCard suit : SuitsCard.values()) {
      for (NumberCard number : NumberCard.values()) {
        listCards.add(new ClassicCard(number, suit));
      }
    }
  }

  public List<ClassicCard> getListCard() {
    return this.listCards;
  }

  public ClassicCard getRandomCard() {
    int cardIndex = this.getRandomNumber();
    ClassicCard cardReturned = listCards.get(cardIndex);
    listCards.remove(cardIndex);
    return cardReturned;
  }

  private int getRandomNumber() {
    int numberGenerate = ThreadLocalRandom
      .current()
      .nextInt(0, this.listCards.size());
    return numberGenerate;
  }
}
