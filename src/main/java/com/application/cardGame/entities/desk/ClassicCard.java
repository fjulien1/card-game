package com.application.cardGame.entities.desk;

import com.application.cardGame.communs.interfaces.Card;
import com.application.cardGame.constants.classicCard.NumberCard;
import com.application.cardGame.constants.classicCard.SuitsCard;

public class ClassicCard extends Card {

  private NumberCard numberCard;
  private SuitsCard suitsCard;

  public ClassicCard(NumberCard numberCard, SuitsCard suitsCard) {
    this.numberCard = numberCard;
    this.suitsCard = suitsCard;
  }

  public Integer getNumberCard() {
    if (this.getIsVisible()) {
      return this.numberCard.value;
    }
    return null;
  }

  public SuitsCard getSuitsCard() {
    if (this.getIsVisible()) {
      return this.suitsCard;
    }
    return null;
  }

  public Integer getSuitsCardValue() {
    if (this.getIsVisible()) {
      return this.suitsCard.value;
    }
    return null;
  }
}
