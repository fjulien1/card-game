package com.application.cardGame.constants.classicCard;

public enum SuitsCard {
  CLUBS(0),
  DIAMONDS(1),
  HEARTS(2),
  SPADES(3);

  public final int value;

  private SuitsCard(int value) {
    this.value = value;
  }
}
