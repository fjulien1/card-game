package com.application.cardGame.communs.interfaces;

public interface CardInterface {
  Card showCard();

  Boolean getIsVisible();

  void setIsVisible(Boolean isVisible);
}
