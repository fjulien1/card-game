package com.application.cardGame.communs.interfaces;

public abstract class Card implements CardInterface {

  private Boolean isVisible = false;

  public Card showCard() {
    this.setIsVisible(true);
    return this;
  }

  public Boolean getIsVisible() {
    return this.isVisible;
  }

  public void setIsVisible(Boolean isVisible) {
    this.isVisible = isVisible;
  }
}
