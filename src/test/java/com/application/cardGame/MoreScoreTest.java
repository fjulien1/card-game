package com.application.cardGame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.application.cardGame.entities.gameRules.games.MoreScore;
import com.application.cardGame.entities.players.PlayerListModel;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class MoreScoreTest {

  @Test
  void initPlayer() {
    PlayerListModel players = new PlayerListModel();
    players.addPlayer("name");
    players.addPlayer("name2");
    players.addPlayer("name3");
    players.addPlayer("name4");

    MoreScore game = new MoreScore(players.getList());

    Assert.isTrue(game instanceof MoreScore, "Initialization game fail");
  }

  @Test
  void emptyPlayer() {
    PlayerListModel players = new PlayerListModel();
    Throwable exception = assertThrows(
      Error.class,
      () -> new MoreScore(players.getList())
    );
    assertEquals(exception.getMessage(), "Size players is incorrect.");
  }

  @Test
  void overPlayers() {
    PlayerListModel players = new PlayerListModel();
    players.addPlayer("name");
    players.addPlayer("name2");
    players.addPlayer("name3");
    players.addPlayer("name4");
    players.addPlayer("name5");

    Throwable exception = assertThrows(
      Error.class,
      () -> new MoreScore(players.getList())
    );
    assertEquals(exception.getMessage(), "Size players is incorrect.");
  }

  @Test
  void getScore() {
    PlayerListModel players = new PlayerListModel();
    players.addPlayer("name");
    players.addPlayer("name2");
    players.addPlayer("name3");
    players.addPlayer("name4");

    MoreScore game = new MoreScore(players.getList());
    game.beginPartyGame();
    game.endPartyGame();
  }
}
