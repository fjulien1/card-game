package com.application.cardGame;

import com.application.cardGame.entities.desk.Desk;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class DeskTest {

  Desk desk = new Desk();

  @Test
  void controlDesk() {
    Assert.isTrue(desk.getListCard().size() == 52, "Size desk is incorrect");
  }

  @Test
  void giveAllCards() {
    int deskSize = desk.getListCard().size();
    for (int i = 0; i < deskSize; i++) {
      Assert.isTrue(
        desk.getListCard().size() == 52 - i,
        "Size desk is incorrect at " + desk.getListCard().size()
      );
      desk.getRandomCard();
    }
    Assert.isTrue(desk.getListCard().size() == 0, "Desk is not empty");
  }

  @Test
  void initDesk() {
    desk.initDesk();
    Assert.isTrue(desk.getListCard().size() == 52, "Size desk is incorrect");
  }
}
