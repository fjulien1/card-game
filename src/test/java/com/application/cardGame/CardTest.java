package com.application.cardGame;

import com.application.cardGame.entities.desk.ClassicCard;
import com.application.cardGame.entities.desk.Desk;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class CardTest {

  Desk desk = new Desk();

  @Test
  void controlCardVisible() {
    ClassicCard card = (ClassicCard) desk.getRandomCard().showCard();
    Assert.notNull(card.getNumberCard(), "Number is null");
    Assert.notNull(card.getSuitsCard(), "Suit is null");
    Assert.notNull(card.getSuitsCardValue(), "Suit value is null");
  }

  @Test
  void controlCardHidden() {
    ClassicCard card = (ClassicCard) desk.getRandomCard();
    Assert.isNull(card.getNumberCard(), "Number is not null");
    Assert.isNull(card.getSuitsCard(), "Suit is not null");
    Assert.isNull(card.getSuitsCardValue(), "Suit value is not null");
  }
}
