# card-game project

### Run Project:

- `./mvnw spring-boot:run;`

### Install project:

- `./mvnw install;`

### Build project:

- `docker build -t card-game . ;`

### Start docker:

- RUN BACKGROUND -> `docker run --rm -d -p 8080:8080 --name card-game card-game;`
- RUN FOREGROUND -> `docker run --rm -ti -p 8080:8080 --name card-game card-game;`

### Stop docker:

- docker stop card-game;

### List docker:

- LIST IMAGES -> `docker image ls;`
- LIST DOCKERS -> `docker ps;`

### Remove image:

- `docker rmi -f ID_IMAGES;`
